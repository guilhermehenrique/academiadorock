﻿using System;
namespace academiadorock.Models
{
    public class Usuario
    {
        public string id { get; set; }
		public string nome { get; set; }
		public string email { get; set; }
		public string senha { get; set; }
		public string status { get; set; }
		public string tipo { get; set; }
		public string erro { get; set; }
		public string mensagem { get; set; }
    }
}
