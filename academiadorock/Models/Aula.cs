﻿using System;
namespace academiadorock.Models
{
    public class Aula
    {
		public string id_aula { get; set; }
		public string dt_aula { get; set; }
		public string hora_ini { get; set; }
		public string hora_fim { get; set; }
		public string reposicao { get; set; }
		public string experimental { get; set; }
		public object aluno_exp { get; set; }
		public string nome_aluno { get; set; }
		public string professor { get; set; }
		public string num_sala { get; set; }
		public string id_statusaula { get; set; }
		public string status { get; set; }
		public string curso { get; set; }
    }
}
