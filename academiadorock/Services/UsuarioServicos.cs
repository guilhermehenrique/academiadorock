﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using academiadorock.Models;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Xamarin.Forms;
using academiadorock.Services;

namespace escoladorock.Servicos
{
	public class UsuarioServicos
	{
		WebService WebService;
		public UsuarioServicos()
		{
			WebService = new WebService();
		}

		public async Task<Usuario> UsuarioWebService(string uni, string email, string senha)
		{
			string url = "http://extranet.academiadorock.com.br/app/valida_login.php";
			uni = "?uni=" + uni;
			email = "&email=" + email;
			senha = "&senha=" + senha;
			url = url + uni + email + senha;
			var usuario = await WebService.UsuarioService(url, uni);
			return usuario;
		}

		public async Task<bool> ValidarUsuario(string sede, string email, string senha)
		{
			Usuario usuario = await UsuarioWebService(sede, email, senha);
			if (usuario.id != null && usuario.status == "Ativo")
			{
				return true;
			}
			return false;
		}

		public async Task<bool> LogarUsuario(string sede, string email, string senha)
		{
			Usuario usuario = await UsuarioWebService(sede, email, senha);
			Application.Current.Properties["email"] = email;
			Application.Current.Properties["senha"] = senha;
			Application.Current.Properties["sede"] = sede;
			Application.Current.Properties["chave"] = usuario.senha;
			Application.Current.Properties["tipo"] = usuario.tipo;
			return true;
		}

	}
}
