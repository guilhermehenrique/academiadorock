﻿
using academiadorock.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using academiadorock.Services;

namespace escoladorock.Servicos
{
	class AulaServicos
	{
		WebService webService;
		public AulaServicos()
		{
			webService = new WebService();
		}

		public async Task<List<Aula>> AulaWebService()
		{
			string url = "http://extranet.academiadorock.com.br/app/consulta_agenda_profe.php";
			string uni = "?uni=" + Application.Current.Properties["sede"];
			string chave = "&chave=" + Application.Current.Properties["chave"];
			url = url + uni + chave;
			var aulas = await webService.AulaService(url);
			return aulas;
		}

		public async Task<List<Aula>> ListarPorDataManha(DateTime dataAula)
		{
			var listaAula = await AulaWebService();
			string data = dataAula.ToString();
			List<Aula> listaResulado = new List<Aula>();

			foreach (var aula in listaAula)
			{
				int hora = int.Parse(aula.hora_ini);
				if (aula.dt_aula == data && hora < 12)
				{
					listaResulado.Add(aula);
				}
			}

			return listaResulado;
		}

		public void ListarPorDataTarde(DateTime data)
		{
			var listaAula = AulaWebService();


		}

		public void ListarPorDataNoite(DateTime data)
		{
			var listaAula = AulaWebService();


		}
	}
}
