﻿using academiadorock.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace academiadorock.Services
{
    public class WebService
    {
		HttpClient client;
		public WebService()
		{
			client = new HttpClient();
		}

		public class ResponseUsuario
		{
			public List<Usuario> usuario { get; set; }
		}

		public async Task<Usuario> UsuarioService(string url, string uni)
		{
			var uri = new Uri(url);
			var response = await client.GetAsync(uri);
			if (response.IsSuccessStatusCode)
			{
				var stream = await response.Content.ReadAsStreamAsync();
				StreamReader reader = new StreamReader(stream, Encoding.UTF8);
				string content = await reader.ReadToEndAsync();
				var resposta = JsonConvert.DeserializeObject<ResponseUsuario>(content);
				Usuario usuario = new Usuario();
				foreach (var u in resposta.usuario) { usuario = u; }
				try
				{
					if (await RegistraChave(uni, usuario.senha, usuario.tipo, usuario.id))
					{
						return usuario;
					}
					else
					{
						return null;
					}
				}
				catch (Exception e)
				{
					string errp = e.ToString();
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		public class ResponseAula
		{
			public List<Aula> aulas { get; set; }
		}

		public async Task<List<Aula>> AulaService(string url)
		{
			var uri = new Uri(url);
			var response = await client.GetAsync(uri);
			if (response.IsSuccessStatusCode)
			{
				var stream = await response.Content.ReadAsStreamAsync();
				StreamReader reader = new StreamReader(stream, Encoding.UTF8);
				string content = await reader.ReadToEndAsync();
				var resposta = JsonConvert.DeserializeObject<ResponseAula>(content);
				return resposta.aulas;
			}
			else
			{
				return null;
			}
		}



		public async Task<bool> RegistraChave(string uni, string chave, string tipo, string id)
		{
			string url = "http://extranet.academiadorock.com.br/app/registra_chave.php";
			chave = "&chave=" + chave;
			tipo = "&tipo=" + tipo;
			id = "&id=" + id;
			url = url + uni + chave + tipo + id;
			var uri = new Uri(url);
			var response = await client.GetAsync(uri);
			if (response.IsSuccessStatusCode)
			{
				var stream = await response.Content.ReadAsStreamAsync();
				StreamReader reader = new StreamReader(stream, Encoding.UTF8);
				string content = await reader.ReadToEndAsync();
				JObject json = new JObject();
				json = JObject.Parse(content);
				string erro = (string)json["usuario"][0]["erro"];
				if (erro == "0")
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
    }
}
