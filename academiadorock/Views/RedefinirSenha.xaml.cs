﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace academiadorock.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RedefinirSenha : ContentPage
	{
		public RedefinirSenha()
		{
			InitializeComponent();
		}

		public async void cancelarClick()
		{
			await Navigation.PopAsync();
		}
	}
}