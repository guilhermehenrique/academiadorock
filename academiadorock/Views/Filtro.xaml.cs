﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace academiadorock.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Filtro : ContentPage
	{
		public Filtro()
		{
			InitializeComponent();
		}

		public void SituacaoClick(object sender, EventArgs args)
		{
			var imagem = (Image)sender;
			string checkbox = imagem.Source.ToString();
			if (checkbox.Contains("blank"))
			{
				checkbox.Replace("blank", "filled");
			}
			else
			{
				checkbox.Replace("filled", "blank");
			}
			imagem.Source = checkbox;
		}

		public async void ServicoClick()
		{
			await Navigation.PushAsync(new FiltroServico());
		}

		public async void SalaClick()
		{
			await Navigation.PushAsync(new FiltroSala());
		}

		public async void FiltrarClick()
		{
			await Navigation.PushAsync(new FiltroResultado());
		}

		public async void SairClick()
		{
			await Navigation.PopToRootAsync();
		}

		public async void PerfilClick()
		{
			await Navigation.PushAsync(new Perfil());
		}

		public async void NotificacaoClick()
		{
			await Navigation.PushAsync(new Notificacoes());
		}
	}
}
