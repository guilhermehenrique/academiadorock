﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace academiadorock.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Perfil : ContentPage
	{
		public Perfil()
		{
			InitializeComponent();
			NavigationPage.SetHasBackButton(this, false);
		}

		public async void NotificacaoClick()
		{
			await Navigation.PushAsync(new Notificacoes());
		}

		public async void AlterarSenhaClick()
		{
			await Navigation.PushAsync(new AlterarSenha());
		}

		public async void AgendaClick()
		{
			await Navigation.PushAsync(new TelaPrincipalAulas());
		}

		public async void SairClick()
		{
			await Navigation.PopToRootAsync();
		}

		public void JuveveClick()
		{
			imgJuveve.Source = "radio_on_red.png";
			imgBatel.Source = "radio_off_red.png";
			imgSantoAndre.Source = "radio_off_red.png";
		}
		public void BatelClick()
		{
			imgJuveve.Source = "radio_off_red.png";
			imgBatel.Source = "radio_on_red.png";
			imgSantoAndre.Source = "radio_off_red.png";
		}
		public void SantoAndreClick()
		{
			imgJuveve.Source = "radio_off_red.png";
			imgBatel.Source = "radio_off_red.png";
			imgSantoAndre.Source = "radio_on_red.png";
		}
	}
}
