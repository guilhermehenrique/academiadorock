﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace academiadorock.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Notificacoes : ContentPage
	{
		public Notificacoes()
		{
			InitializeComponent();
			NavigationPage.SetHasBackButton(this, false);
		}

		public async void AgendaClick()
		{
			await Navigation.PushAsync(new TelaPrincipalAulas());
		}

		public async void PerfilClick()
		{
			await Navigation.PushAsync(new Perfil());
		}

		public async void SairClick()
		{
			await Navigation.PopToRootAsync();
		}
	}
}
