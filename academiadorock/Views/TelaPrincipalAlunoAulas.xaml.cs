﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace academiadorock.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TelaPrincipalAlunoAulas : ContentPage
	{
		public TelaPrincipalAlunoAulas()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}

		public async void AvisosClick()
		{
			await Navigation.PushAsync(new TelaPrincipalAlunoAvisos());

		}

		public async void FiltroClick()
		{
			await Navigation.PushAsync(new Filtro());
		}
	}
}
