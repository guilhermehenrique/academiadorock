﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using escoladorock.Servicos;

namespace academiadorock.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Login : ContentPage
	{
		string Sede;
		UsuarioServicos usuarioServicos;

		public Login()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			Sede = "0";
			usuarioServicos = new UsuarioServicos();
		}

		public async void RedefinirSenhaClick()
		{
			await Navigation.PushAsync(new RedefinirSenha());
		}

		public async void EntrarClick()
		{
			idtLoading.IsVisible = true;
			idtLoading.IsRunning = true;
			var sede = Sede;
			var email = emailEntry.Text;
			var senha = senhaEntry.Text;
			if (sede == null || email == null || senha == null)
			{
				await DisplayAlert("Dados inválidos", "Responda todos os campos!", "OK");
			}
			else
			{
				if (await usuarioServicos.ValidarUsuario(sede, email, senha))
				{
					var logar = await usuarioServicos.LogarUsuario(sede, email, senha);
					Application.Current.Properties["Page"] = "Agenda";
					idtLoading.IsVisible = false;
					idtLoading.IsRunning = false;
					await Navigation.PushAsync(new TelaPrincipalAulas());
				}
				else
				{
					idtLoading.IsVisible = false;
					idtLoading.IsRunning = false;
					await DisplayAlert("Dados inválidos", "Email ou senha incorretos!", "OK");
				}
			}
		}

		public void JuveveClick()
		{
			imgJuveve.Source = ImageSource.FromFile("checked.jpg");
			imgBatel.Source = ImageSource.FromFile("check.jpg");
			imgSantoAndre.Source = ImageSource.FromFile("check.jpg");
			Sede = "1";
		}
		public void BatelClick()
		{
			imgJuveve.Source = ImageSource.FromFile("check.jpg");
			imgBatel.Source = ImageSource.FromFile("checked.jpg");
			imgSantoAndre.Source = ImageSource.FromFile("check.jpg");
			Sede = "2";
		}
		public void SantoAndreClick()
		{
			imgJuveve.Source = ImageSource.FromFile("check.jpg");
			imgBatel.Source = ImageSource.FromFile("check.jpg");
			imgSantoAndre.Source = ImageSource.FromFile("checked.jpg");
			Sede = "3";
		}
	}
}