﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace academiadorock.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TelaPrincipalAlunoAvisos : ContentPage
	{
		public TelaPrincipalAlunoAvisos()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}

		public async void AulasClick()
		{
			await Navigation.PushAsync(new TelaPrincipalAlunoAulas());
		}
	}
}
