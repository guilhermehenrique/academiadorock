﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace academiadorock.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TelaPrincipalAulas : ContentPage
    {
        public TelaPrincipalAulas()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
        }

        public async void FiltroClick()
        {
            await Navigation.PushAsync(new Filtro());
        }

        public async void TurmasClick()
        {
            Navigation.InsertPageBefore(new TelaPrincipalTurmas(), this);
            await Navigation.PopAsync();
        }
    }
}
