﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace academiadorock.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TelaPrincipalTurmas : ContentPage
	{
		public TelaPrincipalTurmas()
		{
			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);

		}

		public async void FiltroClick()
		{
			await Navigation.PushAsync(new Filtro());
		}

		public async void AulasClick()
		{
			Navigation.InsertPageBefore(new TelaPrincipalAulas(), this);
			await Navigation.PopAsync();
		}
	}
}
