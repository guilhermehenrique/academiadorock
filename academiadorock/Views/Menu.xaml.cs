﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace academiadorock.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Menu : StackLayout
	{
		public Menu()
		{
			InitializeComponent();
			if (Application.Current.Properties["tipo"].ToString() == "Aluno")
			{
				stackMenu.Children.Remove(stackNotificacao);

			}
			string pagina = Application.Current.Properties["Page"].ToString();
			if (pagina == "Agenda")
			{
				lblAgenda.TextColor = Color.FromHex("#ffffff");
				imgAgenda.Source = "icon_calendar_white.png";
				imgAgenda.WidthRequest = 16;
				stackAgenda.GestureRecognizers.Clear();
			}
			else if (pagina == "Notificacao")
			{
				lblNotificacao.TextColor = Color.FromHex("#ffffff");
				imgNotificacao.Source = "icon_notification_white.png";
				stackNotificacao.GestureRecognizers.Clear();
			}
			else if (pagina == "Perfil")
			{
				lblPerfil.TextColor = Color.FromHex("#ffffff");
				imgPerfil.Source = "icon_perfil_white.png";
				stackPerfil.GestureRecognizers.Clear();
			}
		}

		public async void AgendaClick()
		{
			Application.Current.Properties["Page"] = "Agenda";
			//await Navigation.PushAsync(new TelaPrincipalAulas(), false);
			await Navigation.PopAsync();
		}

		public async void NotificacaoClick()
		{

			if (Application.Current.Properties["Page"].ToString() != "Agenda")
			{
				Application.Current.Properties["Page"] = "Notificacao";
				Navigation.InsertPageBefore(new Notificacoes(), (Page)Parent.Parent.Parent);
				await Navigation.PopAsync(true);
			}
			Application.Current.Properties["Page"] = "Notificacao";
			await Navigation.PushAsync(new Notificacoes(), true);
		}

		public async void PerfilClick()
		{

			if (Application.Current.Properties["Page"].ToString() != "Agenda")
			{
				Application.Current.Properties["Page"] = "Perfil";
				Navigation.InsertPageBefore(new Perfil(), (Page)Parent.Parent.Parent);
				await Navigation.PopAsync(true);
			}
			Application.Current.Properties["Page"] = "Perfil";
			await Navigation.PushAsync(new Perfil(), true);
		}

		public async void SairClick()
		{
			await Navigation.PopToRootAsync();
		}
	}
}
