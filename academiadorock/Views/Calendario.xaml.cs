﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace academiadorock.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Calendario : StackLayout
	{
		Label old;
		int gdDia = 0;
		int gdMes;
		int gdAno;
		public Calendario()
		{
			InitializeComponent();

            CultureInfo pt = CultureInfo.CurrentCulture;
            Calendar calendario = pt.Calendar; 
                
			DateTime data = DateTime.Now;
			int mes = data.Month;
			int ano = data.Year;
			int dia = data.Day;
			MontaCalendario(mes, ano, dia);
			MontaMesAno(mes, ano);
		}

		public void MontaMesAno(int mes, int ano)
		{
			try
			{
				switch (mes)
				{
					case 1: lblMesFachada.Text = "Janeiro"; break;
					case 2: lblMesFachada.Text = "Fevereiro"; break;
					case 3: lblMesFachada.Text = "Março"; break;
					case 4: lblMesFachada.Text = "Abril"; break;
					case 5: lblMesFachada.Text = "Maio"; break;
					case 6: lblMesFachada.Text = "Junho"; break;
					case 7: lblMesFachada.Text = "Julho"; break;
					case 8: lblMesFachada.Text = "Agosto"; break;
					case 9: lblMesFachada.Text = "Setembro"; break;
					case 10: lblMesFachada.Text = "Outubro"; break;
					case 11: lblMesFachada.Text = "Novembro"; break;
					case 12: lblMesFachada.Text = "Dezembro"; break;
					default: break;
				}

				lblMes.Text = mes.ToString();
				lblAno.Text = ano.ToString();
			}
			catch (Exception e)
			{
				string erro = e.ToString();
				erro = e.ToString();
			}

		}

		public void AvancarMes(object sender, EventArgs ev)
		{
			try
			{
				int mes = int.Parse(lblMes.Text);
				int ano = int.Parse(lblAno.Text);


				if (mes == 12)
				{
					ano++;
					mes = 1;
				}
				else
				{
					mes++;
				}

				MontaMesAno(mes, ano);
				MontaCalendario(mes, ano, null);

			}
			catch (Exception e)
			{
				string erro = e.ToString();
				erro = e.ToString();
			}

		}

		public void VoltarMes(object sender, EventArgs ev)
		{
			try
			{
				int mes = int.Parse(lblMes.Text);
				int ano = int.Parse(lblAno.Text);

				if (mes == 1)
				{
					ano--;
					mes = 12;
				}
				else
				{
					mes--;
				}

				MontaMesAno(mes, ano);
				MontaCalendario(mes, ano, null);
			}
			catch (Exception e)
			{
				string erro = e.ToString();
				erro = e.ToString();
			}

		}

		public void MontaCalendario(int mes, int ano, int? dia)
		{
            CultureInfo pt = CultureInfo.CurrentCulture;
            Calendar calendario = pt.Calendar;

			int daysInMonth = calendario.GetDaysInMonth(ano, mes);

			var grid = new Grid();
			grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
			grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
			grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
			grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
			grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
			grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

			grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

			int countDias = 1;
			string primeiro;
			if (mes < 10)
			{
				primeiro = ano + "-0" + mes + "-01" + " 00:00";
			}
			else
			{
				primeiro = ano + "-" + mes + "-01" + " 00:00";
			}

			DateTime primeiroDia = new DateTime();
			primeiroDia = DateTime.ParseExact(primeiro, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);

			int weekColumn = (int)primeiroDia.DayOfWeek;

			for (int x = 0; x <= 5; x++)
			{

				for (int y = weekColumn; y <= 6; y++)
				{
					var label = new Label();
					if (countDias < 10)
					{
						label.Text = "0" + countDias.ToString();
					}
					else
					{
						label.Text = countDias.ToString();
					}
					label.TextColor = Color.FromHex("#000000");
					label.Margin = 5;
					var stackDia = new StackLayout();
					stackDia.Children.Add(label);
					stackDia.HorizontalOptions = LayoutOptions.CenterAndExpand;
					stackDia.VerticalOptions = LayoutOptions.FillAndExpand;
					if (countDias == dia)
					{
						label.BackgroundColor = Color.FromHex("#cb4233");
						label.Margin = 5;
						label.TextColor = Color.FromHex("#ffffff");
						stackDia.BackgroundColor = Color.FromHex("#cb4233");
						old = label;
					}
					label.HorizontalOptions = LayoutOptions.CenterAndExpand;
					label.VerticalOptions = LayoutOptions.FillAndExpand;



					var onTap = new TapGestureRecognizer();
					onTap.Tapped += (s, e) =>
					{
						var st = (StackLayout)s;
						var lbl = (Label)st.Children.First();
						lbl.TextColor = Color.FromHex("#ffffff");
						lbl.BackgroundColor = Color.FromHex("#cb4233");
						lbl.Margin = 5;

						var sl = (StackLayout)lbl.Parent;
						sl.BackgroundColor = Color.FromHex("#cb4233");

						if (old != null)
						{
							var oldSl = (StackLayout)old.Parent;
							oldSl.BackgroundColor = Color.FromHex("#ffffff");
							old.BackgroundColor = Color.FromHex("#ffffff");
							old.TextColor = Color.FromHex("#000000");
						}

						old = lbl;
					};

					stackDia.GestureRecognizers.Add(onTap);


					grid.Children.Add(stackDia, y, x);
					countDias++;
					if (countDias > daysInMonth)
					{
						break;
					}
				}
				if (countDias > daysInMonth)
				{
					break;
				}
				weekColumn = 0;
			}

			var layout = (StackLayout)stackCalendario;
			layout.Children.Clear();
			layout.Children.Add(grid);
		}

	}

}